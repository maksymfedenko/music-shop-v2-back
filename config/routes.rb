Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'

  scope :api do
    resources :products
    resources :categories
    resources :brands
    resources :rates
    resources :order_items
    resources :orders
    resources :favorites
    resources :albums
    resources :songs
    resources :genre_relations
    resources :genres
    mount_devise_token_auth_for 'User', at: 'auth', controllers: {
      confirmations: 'overrides/confirmations',
      registrations: 'overrides/registrations',
      sessions: 'overrides/sessions',
      token_validations: 'overrides/token_validations',
      passwords: 'overrides/passwords',
    }

    resources :authors, only: [:index, :show] do
      scope module: :authors do
        resources :albums, only: :index
        resources :songs, only: :index
      end
    end
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  end
end
