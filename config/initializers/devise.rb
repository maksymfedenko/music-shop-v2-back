Devise.setup do |config|
  config.secret_key = ENV['devise_secret_key']
  config.mailer = 'UserMailer'
end
