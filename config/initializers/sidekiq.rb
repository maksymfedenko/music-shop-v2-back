require 'sidekiq'
require 'sidekiq/web'

uri = ENV['REDISTOGO_URL']
REDIS = Redis.new(:url => uri)

Sidekiq.configure_client do |config|
  config.redis = { size: 1 }
end

Sidekiq::Web.use(Rack::Auth::Basic) do |user, password|
  [user, password] == [ ENV['sidekiq_login'], ENV['sidekiq_password'] ]
end
