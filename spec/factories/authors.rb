FactoryGirl.define do
  factory :author do
    transient do
      photos_count (1..3).to_a.sample
    end

    name { { en: Faker::Book.author } }
    description { { en: Faker::Book.author } }
    image { sample_image('authors') }
    photos { photos_count.times.map { sample_image('authors') } }
    link { 'http://attackofthecute.com' }
  end
end
