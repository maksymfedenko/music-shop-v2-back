FactoryGirl.define do
  factory :order_item do
    order
    amount { Faker::Number.between(1, 100) }
    sell_price { Faker::Number.between(1, 100) }
    comment { Faker::Lorem.paragraph }

    trait :with_song do
      subject { create(:song, :with_album) }
      #product_type 'Song'
      #product_id { create(:song, :with_album).id }
    end

    trait :with_album do
      subject { create(:album) }
      #product_type 'Album'
      #product_id { create(:album).id }
    end

  end
end
