FactoryGirl.define do
  factory :user do
    name { Faker::Name.first_name }
    email { Faker::Internet.email }
    password '123123123'
    password_confirmation '123123123'
    confirmed_at { Time.now }
    image { sample_image('users') }

    factory :user_for_create do
      confirmed_at nil
    end
  end
end
