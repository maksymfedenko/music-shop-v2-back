FactoryGirl.define do
  factory :category do
    name { { en: Faker::Overwatch.location } }
    image { sample_image('categories') }
  end
end
