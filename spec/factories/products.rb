FactoryGirl.define do
  factory :product do
    transient do
      photos_count (1..3).to_a.sample
    end

    brand
    category
    name { { en: Faker::StarWars.character } }
    price { Faker::Commerce.price.round }
    year { Faker::Number.between(1900, 2017) }
    image { sample_image('products') }
    photos { photos_count.times.map { sample_image('products') } }
    rate { Faker::Number.between(0, 5) }
    description { { en: Faker::Overwatch.quote } }
  end
end
