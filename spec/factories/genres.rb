FactoryGirl.define do
  factory :genre do
    name { { en: Faker::Book.genre } }
  end
end
