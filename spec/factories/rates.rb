FactoryGirl.define do
  factory :rate do
    user

    trait :with_product do
      subject { create(:product) }
    end

    trait :with_album do
      subject { create(:album) }
    end
  end
end