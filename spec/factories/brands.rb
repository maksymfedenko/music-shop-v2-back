FactoryGirl.define do
  factory :brand do
    name { { en: Faker::Company.name } }
    image { sample_image('brands') }
  end
end
