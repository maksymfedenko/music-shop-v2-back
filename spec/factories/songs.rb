FactoryGirl.define do
  factory :song do
    album
    audio { sample_audio('songs') }
    name { { en: Faker::Overwatch.hero } }
  end
end
