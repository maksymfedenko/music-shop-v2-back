FactoryGirl.define do
  factory :genre_relation do
    genre

    trait :with_author do
      subject { create(:author) }
      #subject_type 'Author'
      #subject_id { create(:author).id }
    end

    trait :with_album do
      subject { create(:album) }
      #subject_type 'Album'
      #subject_id { create(:album).id }
    end
  end
end