FactoryGirl.define do
  factory :favorite do
    user

    trait :with_album do
      subject_type 'Album'
      subject_id { create(:album).id }
    end
  end
end
