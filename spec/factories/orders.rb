FactoryGirl.define do
  factory :order do
    pay_type { Order.pay_types.sample }
    comment { Faker::Lorem.paragraph }

    trait :with_user do
      user_id { create(:user).id }
    end

    trait :with_new_user do
      user_attributes { attributes_for(:user) }
    end

    factory :order_with_items do
      transient do
        items_count 5
      end

      after(:create) do |order, evaluator|
        create_list(:order_item, evaluator.items_count, order: order)
      end
    end
  end
end
