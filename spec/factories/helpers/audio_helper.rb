module AudioHelper
  def sample_audio(folder_name)
    rack_test_audio_file(sample_file_path(folder_name))
  end

  private

  def files_directory_path
    File.expand_path(File.join('..', 'files', 'audio'), File.dirname(__FILE__))
  end

  def rack_test_audio_file(file_path)
    Rack::Test::UploadedFile.new(File.open(file_path), 'audio/mpeg')
  end

  def sample_file_path(folder_name)
    Dir[File.join(files_directory_path, folder_name.to_s) + '/*'].sample
  end
end

FactoryGirl::SyntaxRunner.send(:include, AudioHelper)
