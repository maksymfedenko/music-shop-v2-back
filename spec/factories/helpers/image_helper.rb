module ImageHelper
  def image_instance(relative_file_path)
    rack_test_image_file(File.join(files_directory_path, relative_file_path))
  end

  def sample_image(folder_name)
    rack_test_image_file(sample_file_path(folder_name))
  end

  private

  def files_directory_path
    File.expand_path(File.join('..', 'files', 'images'), File.dirname(__FILE__))
  end

  def rack_test_image_file(file_path)
    Rack::Test::UploadedFile.new(File.open(file_path), 'image/jpeg')
  end

  def sample_file_content(folder_name)
    File.open(sample_file_path(folder_name), 'rb') {|f| f.read}
  end

  def sample_file_path(folder_name)
    Dir[File.join(files_directory_path, folder_name.to_s) + '/*'].sample
  end
end

FactoryGirl::SyntaxRunner.send(:include, ImageHelper)
