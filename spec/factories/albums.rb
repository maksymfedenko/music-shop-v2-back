FactoryGirl.define do
  factory :album do
    author
    name { { en: Faker::Book.title } }
    price { Faker::Commerce.price.round }
    year { Faker::Number.between(1900, 2017) }
    image { sample_image('albums') }
    rate { Faker::Number.between(0, 5) }
  end
end
