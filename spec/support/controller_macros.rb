module ControllerMacros
  def login_user(factory_params = {})
    let(:current_user) { FactoryGirl.create(:user, factory_params) }
    let(:auth_headers) { current_user.create_new_auth_token }
    before(:each) { request.headers.merge! auth_headers if request }
  end
end