class AuthorSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :link, :image, :photos
  has_many :genre_relations
  has_many :albums
end
