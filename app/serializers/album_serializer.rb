class AlbumSerializer < ActiveModel::Serializer
  attributes :id, :name, :price, :image, :year, :rate
  has_one :author
  has_many :genre_relations

  class AuthorSerializer < ActiveModel::Serializer
    attributes :id, :name, :image
  end
end
