class GenreRelationSerializer < ActiveModel::Serializer
  attributes :id, :name, :genre_id

  def name
    object.genre.name
  end
end
