class LocationSerializer < ActiveModel::Serializer
  attributes :id, :city, :country, :city_place_id, :country_place_id, :country_code
end
