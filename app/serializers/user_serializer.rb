class UserSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :last_name, :image, :email, :phone, :birthdate, :gender
  has_one :location
  has_many :genre_relations

  class GenreRelationSerializer < ActiveModel::Serializer
    attributes :id
    has_one :genre
  end
end
