class AuthorsController < ApplicationController

  def show
    authorize Author
    render_resource_or_errors Author.find params[:id]
  end
end
