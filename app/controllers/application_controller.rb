class ApplicationController < ActionController::API
  include ActionController::Helpers
  include ActionController::Serialization

  include DeviseTokenAuth::Concerns::SetUserByToken
  include Pundit

  include BaseControllerMethods
  before_action :set_locale
  before_action :configure_permitted_parameters, if: :devise_controller?
  after_action :verify_authorized, unless: :devise_controller?

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: %w(email password password_confirmation))
    devise_parameter_sanitizer.permit(:account_update, keys: [:image, :first_name, :last_name, :phone, :birthdate, :gender,
                                                              location_attributes: [:id, :city, :city_place_id, :country, :country_place_id, :country_code],
                                                              genre_relations_attributes: [:id, :genre_id, :_destroy]
    ])
  end

  def permitted_attributes(record)
    params.require(:resource).permit(policy(record).permitted_attributes)
  end

  def set_locale
    I18n.locale = request.cookies['X-Locale'] || I18n.default_locale
  end

  rescue_from ActionController::ParameterMissing do |exception|
    render json: { errors: exception.message }, status: :bad_request
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    render json: { errors: exception.message }, status: :not_found
  end

  rescue_from Pundit::NotAuthorizedError do
    render json: { errors: 'You are not authorized to access this action.' }, status: :forbidden
  end
end
