class Overrides::SessionsController < DeviseTokenAuth::SessionsController

  def render_create_success
    render_resource_data @resource
  end
end
