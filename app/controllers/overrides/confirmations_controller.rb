module Overrides
  class ConfirmationsController < DeviseTokenAuth::ConfirmationsController
    def show
      @resource = resource_class.confirm_by_token(params[:confirmation_token])

      if @resource and @resource.id
        # create client id
        client_id  = SecureRandom.urlsafe_base64(nil, false)
        token      = SecureRandom.urlsafe_base64(nil, false)
        token_hash = BCrypt::Password.create(token)
        expiry     = (Time.now + DeviseTokenAuth.token_lifespan).to_i

        @resource.tokens[client_id] = {
            token:  token_hash,
            expiry: expiry
        }

        @resource.save!

        yield @resource if block_given?

        auth_header = @resource.create_new_auth_token

        # update the response header
        response.headers.merge!(auth_header)

        render json: @resource, root: :resource
      else
        raise ActionController::RoutingError.new('Not Found')
      end
    end
  end
end
