class Overrides::TokenValidationsController < DeviseTokenAuth::TokenValidationsController

  def render_validate_token_success
    render_resource_data @resource
  end
end
