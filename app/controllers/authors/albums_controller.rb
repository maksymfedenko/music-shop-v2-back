class Authors::AlbumsController < ApplicationController
  expose :author, id: :author_id, model: Author
  skip_after_action :verify_authorized

  def index
    render_resources author.albums, each_serializer: AlbumSerializer
  end
end