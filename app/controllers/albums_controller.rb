class AlbumsController < ApplicationController

  def index
    authorize Album
    albums = Album.page(params[:page]).per(params[:per_page]).includes(:author, genre_relations: [:genre])
    render_resources albums, each_serializer: AlbumSerializer
  end

  def show
    authorize Album
    render_resource_or_errors Album.find params[:id]
  end
end
