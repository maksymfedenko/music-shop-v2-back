class GenresController < ApplicationController

  def index
    authorize Genre
    render_resources Genre.all, each_serializer: GenreSerializer
  end
end
