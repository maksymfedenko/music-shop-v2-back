class TempImagesController < ApplicationController

  def create
    @image = TempImage.new(image_params)
    @image.save
    render_resource_or_errors(@image)
  end

  private

  def image_params
    params.require(:resource).permit(:image)
  end
end
