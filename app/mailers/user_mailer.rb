class UserMailer < Devise::Mailer
  include Devise::Controllers::UrlHelpers # Optional. eg. `confirmation_url`

  def confirmation_instructions(record, token, opts={})
    @confirmation_url = confirmation_url(record, {confirmation_token: token, redirect_url: "http://#{ENV['host']}:#{ENV['port']}"}).gsub(/\/api\/auth/, '')
    super
  end
end
