class Product < ApplicationRecord
  belongs_to :brand
  belongs_to :category
  has_many :favorites, as: :subject
  has_many :order_items, as: :subject
  has_many :rates, as: :subject

  mount_uploader :image, BaseUploader
  mount_uploaders :photos, BaseUploader
  include TmpUploaders

  enable_uploaders(:image, :photos)
end
