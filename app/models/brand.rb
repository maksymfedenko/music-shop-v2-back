class Brand < ApplicationRecord
  has_many :products

  mount_uploader :image, BaseUploader
  include TmpUploaders

  enable_uploaders(:image)
end
