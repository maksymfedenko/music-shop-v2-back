class TempImage < ApplicationRecord
  validates :image, presence: true

  mount_uploader :image, BaseUploader

  class << self
    def file_instances(ids)
      images = [self.find(ids)].flatten
      files = images.map{ |temp_image| File.open(temp_image.image.file.file) }

      ids.is_a?(Array) ? files : files.first
    end

    alias_method :file_instance, :file_instances
  end
end
