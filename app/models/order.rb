class Order < ApplicationRecord
  belongs_to :user
  has_many :order_items

  enum pay_type: %w(cash card)
end
