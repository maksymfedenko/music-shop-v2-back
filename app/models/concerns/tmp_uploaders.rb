module TmpUploaders
  extend ActiveSupport::Concern

  included do
    def self.enable_uploaders(*args)
      args.each do |uploader|
        next unless self.uploaders.include?(uploader)
        uploader = uploader.to_s
        define_method "removing_#{uploader}=" do |indexes|
          self.public_send("#{uploader}=", self.send(uploader).reject.with_index{|photo, i| i.in?(Array(indexes)) && photo.remove!})
        end

        define_method "#{uploader}_temp_image_id#{'s' if uploader.pluralize == uploader}=" do |ids|
          self.public_send("#{uploader}=", self.send(uploader).kind_of?(Array) ?
              (self.send(uploader) + TempImage.file_instances(ids)) : TempImage.file_instances(ids))
        end
      end
    end
  end
end
