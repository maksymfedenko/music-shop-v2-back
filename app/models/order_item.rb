class OrderItem < ApplicationRecord
  belongs_to :order
  belongs_to :subject, polymorphic: true
end
