class GenreRelation < ApplicationRecord
  belongs_to :genre
  belongs_to :subject, polymorphic: true
end
