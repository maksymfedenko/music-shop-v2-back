class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  has_many :genre_relations, as: :subject
  has_many :genres, through: :genre_relations
  has_many :favorites
  has_many :orders
  has_many :rates
  has_one :location

  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :genre_relations, allow_destroy: true

  mount_uploader :image, BaseUploader
  include TmpUploaders

  enable_uploaders(:image)

  enum gender: %i(male female)

  after_commit :send_devise_pending_notifications

  def send_devise_notification(notification, *args)
    if changed?
      devise_pending_notifications << [ notification, args ]
    else
      devise_mailer.send(notification, self, *args).deliver_later
    end
  end

  def send_devise_pending_notifications
    devise_pending_notifications.each do |notification, args|
      devise_mailer.send(notification, self, *args).deliver_later
    end
    @devise_pending_notifications = []
  end

  def devise_pending_notifications
    @devise_pending_notifications ||= []
  end
end
