class Song < ApplicationRecord
  belongs_to :album
  has_many :favorites, as: :subject
  has_many :genre_relations, as: :subject
end
