class Album < ApplicationRecord
  belongs_to :author
  has_many :songs
  has_many :favorites, as: :subject
  has_many :genre_relations, as: :subject
  has_many :order_items, as: :subject
  has_many :rates, as: :subject

  mount_uploader :image, BaseUploader
  include TmpUploaders

  enable_uploaders(:image)
end
