class Author < ApplicationRecord
  has_many :albums
  has_many :songs, through: :albums
  has_many :favorites, as: :subject
  has_many :genre_relations, as: :subject

  mount_uploader :image, BaseUploader
  mount_uploaders :photos, BaseUploader
  include TmpUploaders

  enable_uploaders(:image, :photos)
end
