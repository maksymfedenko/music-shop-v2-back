DatabaseCleaner.strategy = :truncation, { except: %w(public.schema_migrations) }
DatabaseCleaner.clean
p 'Data cleaned'

FactoryGirl.create :user, email: 'lol@m.ua'
p 'Users created'

FactoryGirl.create_list :author, 10
p 'Authors created'

FactoryGirl.create :genre, name: { en: 'Rock', ru: 'Рок' }
FactoryGirl.create :genre, name: { en: 'Pop', ru: 'Поп' }
FactoryGirl.create :genre, name: { en: 'Trance', ru: 'Транс' }
p 'Genres created'

Author.all.each do |author|
  (1..4).to_a.sample.times {
    album = FactoryGirl.create :album, author: author
    Genre.all.sample((1..3).to_a.sample).each { |genre| FactoryGirl.create :genre_relation, genre: genre, subject: album }
  }

  Genre.all.sample((1..3).to_a.sample).each { |genre| FactoryGirl.create :genre_relation, genre: genre, subject: author }
end
p 'Albums created'

#Album.all.each do |album|
#  rand(10).times { FactoryGirl.create :song, album: album }
#end
