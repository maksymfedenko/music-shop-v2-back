# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170512152635) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "albums", force: :cascade do |t|
    t.decimal  "price"
    t.integer  "year"
    t.string   "image"
    t.decimal  "rate"
    t.integer  "author_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb    "name"
    t.index ["author_id"], name: "index_albums_on_author_id", using: :btree
  end

  create_table "authors", force: :cascade do |t|
    t.string   "link"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.json     "photos"
    t.jsonb    "name"
    t.jsonb    "description"
  end

  create_table "brands", force: :cascade do |t|
    t.string   "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb    "name"
  end

  create_table "categories", force: :cascade do |t|
    t.string   "image"
    t.integer  "parent_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb    "name"
  end

  create_table "favorites", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "subject_type"
    t.integer  "subject_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["subject_type", "subject_id"], name: "index_favorites_on_subject_type_and_subject_id", using: :btree
    t.index ["user_id"], name: "index_favorites_on_user_id", using: :btree
  end

  create_table "genre_relations", force: :cascade do |t|
    t.integer  "genre_id"
    t.string   "subject_type"
    t.integer  "subject_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["genre_id"], name: "index_genre_relations_on_genre_id", using: :btree
    t.index ["subject_type", "subject_id"], name: "index_genre_relations_on_subject_type_and_subject_id", using: :btree
  end

  create_table "genres", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb    "name"
  end

  create_table "locations", force: :cascade do |t|
    t.string  "city"
    t.string  "country"
    t.string  "city_place_id"
    t.string  "country_place_id"
    t.string  "country_code"
    t.integer "user_id"
    t.index ["user_id"], name: "index_locations_on_user_id", using: :btree
  end

  create_table "order_items", force: :cascade do |t|
    t.string   "comment"
    t.string   "sell_price"
    t.integer  "order_id"
    t.integer  "amount"
    t.string   "subject_type"
    t.integer  "subject_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["order_id"], name: "index_order_items_on_order_id", using: :btree
    t.index ["subject_type", "subject_id"], name: "index_order_items_on_subject_type_and_subject_id", using: :btree
  end

  create_table "orders", force: :cascade do |t|
    t.string   "comment"
    t.string   "pay_type"
    t.integer  "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_orders_on_user_id", using: :btree
  end

  create_table "products", force: :cascade do |t|
    t.integer  "brand_id"
    t.integer  "category_id"
    t.integer  "year"
    t.string   "image"
    t.decimal  "rate"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.json     "photos"
    t.jsonb    "name"
    t.jsonb    "description"
    t.index ["brand_id"], name: "index_products_on_brand_id", using: :btree
    t.index ["category_id"], name: "index_products_on_category_id", using: :btree
  end

  create_table "rates", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "subject_type"
    t.integer  "subject_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["subject_type", "subject_id"], name: "index_rates_on_subject_type_and_subject_id", using: :btree
    t.index ["user_id"], name: "index_rates_on_user_id", using: :btree
  end

  create_table "songs", force: :cascade do |t|
    t.integer  "album_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb    "name"
    t.index ["album_id"], name: "index_songs_on_album_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "provider",               default: "email", null: false
    t.string   "uid",                    default: "",      null: false
    t.string   "encrypted_password",     default: "",      null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,       null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "name"
    t.string   "nickname"
    t.string   "image"
    t.string   "email"
    t.json     "tokens"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone"
    t.datetime "birthdate"
    t.integer  "gender"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true, using: :btree
  end

  add_foreign_key "albums", "authors"
  add_foreign_key "favorites", "users"
  add_foreign_key "genre_relations", "genres"
  add_foreign_key "locations", "users"
  add_foreign_key "order_items", "orders"
  add_foreign_key "orders", "users"
  add_foreign_key "products", "brands"
  add_foreign_key "products", "categories"
  add_foreign_key "rates", "users"
  add_foreign_key "songs", "albums"
end
