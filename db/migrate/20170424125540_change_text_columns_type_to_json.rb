class ChangeTextColumnsTypeToJson < ActiveRecord::Migration[5.0]
  def up
    remove_column :albums, :name
    remove_column :authors, :name
    remove_column :authors, :description
    remove_column :brands, :name
    remove_column :categories, :name
    remove_column :genres, :name
    remove_column :products, :name
    remove_column :products, :description
    remove_column :songs, :name

    add_column :albums, :name, :jsonb
    add_column :authors, :name, :jsonb
    add_column :authors, :description, :jsonb
    add_column :brands, :name, :jsonb
    add_column :categories, :name, :jsonb
    add_column :genres, :name, :jsonb
    add_column :products, :name, :jsonb
    add_column :products, :description, :jsonb
    add_column :songs, :name, :jsonb
  end

  def down
    remove_column :albums, :name
    remove_column :authors, :name
    remove_column :authors, :description
    remove_column :brands, :name
    remove_column :categories, :name
    remove_column :genres, :name
    remove_column :products, :name
    remove_column :products, :description
    remove_column :songs, :name

    add_column :albums, :name, :string
    add_column :authors, :name, :string
    add_column :authors, :description, :string
    add_column :brands, :name, :string
    add_column :categories, :name, :string
    add_column :genres, :name, :string
    add_column :products, :name, :string
    add_column :products, :description, :string
    add_column :songs, :name, :string
  end
end
