class CreateAlbums < ActiveRecord::Migration[5.0]
  def change
    create_table :albums do |t|
      t.string :name
      t.decimal :price
      t.integer :year
      t.string :image
      t.decimal :rate
      t.references :author, foreign_key: true

      t.timestamps
    end
  end
end
