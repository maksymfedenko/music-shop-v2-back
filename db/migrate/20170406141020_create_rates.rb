class CreateRates < ActiveRecord::Migration[5.0]
  def change
    create_table :rates do |t|
      t.references :user, foreign_key: true
      t.references :subject, polymorphic: true

      t.timestamps
    end
  end
end
