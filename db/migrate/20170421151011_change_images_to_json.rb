class ChangeImagesToJson < ActiveRecord::Migration[5.0]
  def change
    add_column :authors, :photos, :json
    add_column :products, :photos, :json
  end
end
