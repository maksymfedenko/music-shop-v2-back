class CreateLocation < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.string :city
      t.string :country
      t.string :city_place_id
      t.string :country_place_id
      t.string :country_code
      t.references :user, foreign_key: true
    end
  end
end
