class CreateGenreRelations < ActiveRecord::Migration[5.0]
  def change
    create_table :genre_relations do |t|
      t.references :genre, foreign_key: true
      t.references :subject, polymorphic: true

      t.timestamps
    end
  end
end
