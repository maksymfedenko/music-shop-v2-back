class CreateAuthors < ActiveRecord::Migration[5.0]
  def change
    create_table :authors do |t|
      t.string :name
      t.string :link
      t.string :image
      t.string :description

      t.timestamps
    end
  end
end
