class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.references :brand, foreign_key: true
      t.references :category, foreign_key: true
      t.string :name
      t.integer :year
      t.string :description
      t.string :image
      t.decimal :rate

      t.timestamps
    end
  end
end
