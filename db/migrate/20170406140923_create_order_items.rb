class CreateOrderItems < ActiveRecord::Migration[5.0]
  def change
    create_table :order_items do |t|
      t.string :comment
      t.string :sell_price
      t.references :order, foreign_key: true
      t.integer :amount
      t.references :subject, polymorphic: true

      t.timestamps
    end
  end
end
